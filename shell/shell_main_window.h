/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined SHELLMAINWINDOW_H
#define SHELLMAINWINDOW_H

#include <KDE/KParts/MainWindow>

class TrainCommandMainWindow : public KParts::MainWindow
{

    Q_OBJECT

public:

    TrainCommandMainWindow(QWidget *parent = 0, Qt::WindowFlags f = 0);

    virtual ~TrainCommandMainWindow();

signals:

    void addNewApplet(const QString& name);

public Q_SLOTS:
    void optionsPreferences();

    void addClockApplet();

protected:

    void loadKPart(const QVariantList &args);

private:

    KParts::ReadOnlyPart *m_part;
};

#endif // SHELLMAINWINDOW_H
