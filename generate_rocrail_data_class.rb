#! /usr/bin/env ruby

#   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU Library General Public License as
#   published by the Free Software Foundation; either version 3, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details
#
#   You should have received a copy of the GNU Library General Public
#   License along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require 'generate_rocrail_data_class_source'

CodeGenerator.generateClass do |modelDefinition|
    modelDefinition.className = 'ClockTick'
    modelDefinition.xmlClassName = 'clock'
    modelDefinition.add_scalar 'mDivider', 'int', 'divider', nil, nil, nil
    modelDefinition.add_scalar 'mClockTime', 'qlonglong', 'clockTime', 'setClockTime', 'time', nil
    modelDefinition.add_enum 'mCommand', 'Command', 'command', nil, ['sync', 'invalid'], 'cmd', nil
end

CodeGenerator.generateMixedModelDeclarationClass do |modelDefinition|
    modelDefinition.className = 'ModelQuery'
    modelDefinition.classDocumentation = [
        'Class that is used to represent a query or command about RocRail models.',
        '',
        'It takes a Command parameter indicating which query or command is to be sent with this model block.']
    modelDefinition.xmlClassName = 'model'
    modelDefinition.add_enum 'mCommand', 'Command', 'command', nil,
                             ['add', 'addmodule', 'bklist', 'fstat', 'initfield', 'lclist', 'lcprops', 'merge', 'modify', 'move',
                              'plan', 'plantitle', 'remove', 'save', 'sclist', 'stlist', 'swlist', 'themes', 'invalid'], 'cmd',
                              'The actual command set for this ModelQuery object.'
    modelDefinition.add_child 'mLocomotive', 'Locomotive', 'lc', 'locomotive', 'setLocomotive', nil
end

CodeGenerator.generateClass do |modelDefinition|
    modelDefinition.className = 'Locomotive'
    modelDefinition.xmlClassName = 'lc'
    modelDefinition.add_scalar 'mId', 'QString', 'id', nil, nil, nil
    modelDefinition.add_scalar 'mPreviousId', 'QString', 'previousId', 'setPreviousId', 'prev_id', nil
    modelDefinition.add_scalar 'mShortId', 'QString', 'shortId', 'setShortId', 'shortid', nil
    modelDefinition.add_scalar 'mCurrentSpeed', 'int', 'speed', nil, 'V', nil
    modelDefinition.add_scalar 'mLightOn', 'bool', 'lightOn', 'setLightOn', 'fn', nil
    modelDefinition.add_scalar 'mDirectionIsForward', 'bool', 'directionIsForward', 'setDirectionIsForward', 'dir', nil
    modelDefinition.add_scalar 'mMinSpeed', 'int', 'minSpeed', 'setMinSpeed', 'V_min', nil
    modelDefinition.add_scalar 'mMediumSpeed', 'int', 'mediumSpeed', 'setMediumSpeed', 'V_mid', nil
    modelDefinition.add_scalar 'mMaxSpeed', 'int', 'maxSpeed', 'setMaxSpeed', 'V_max', nil
    modelDefinition.add_scalar 'mReverseMinSpeed', 'int', 'reverseMinSpeed', 'setReverseMinSpeed', 'V_Rmin', nil
    modelDefinition.add_scalar 'mReverseMediumSpeed', 'int', 'reverseMediumSpeed', 'setReverseMediumSpeed', 'V_Rmid', nil
    modelDefinition.add_scalar 'mReverseMaxSpeed', 'int', 'reverseMaxSpeed', 'setReverseMaxSpeed', 'V_Rmax', nil
    modelDefinition.add_scalar 'mUseScheduleTime', 'bool', 'useScheduleTime', 'setUseScheduleTime', 'usescheduletime', nil
    modelDefinition.add_scalar 'mShow', 'bool', 'show', nil, nil, nil
    modelDefinition.add_scalar 'mBus', 'int', 'bus', nil, nil, nil
    modelDefinition.add_scalar 'mDescription', 'QString', 'description', nil, 'desc', nil
    modelDefinition.add_scalar 'mDocumentation', 'QString', 'documentation', nil, 'docu', nil
    modelDefinition.add_scalar 'mAdress', 'int', 'adress', nil, 'addr', nil
    modelDefinition.add_scalar 'mRoadName', 'QString', 'roadName', 'setRoadName', 'roadname', nil
    modelDefinition.add_scalar 'mLength', 'int', 'length', nil, 'len', nil
    modelDefinition.add_scalar 'mIdentifier', 'int', 'identifier', nil, nil, nil
    modelDefinition.add_scalar 'mUseShortId', 'bool', 'useShortId', 'setUseShortId', 'useshortid', nil
    modelDefinition.add_scalar 'mProtocol', 'QString', 'protocol', nil, 'prot', nil
    modelDefinition.add_scalar 'mProtocolVersion', 'int', 'protocolVersion', 'setProtocolVersion', 'protver', nil
    modelDefinition.add_scalar 'mInterfaceId', 'QString', 'interfaceId', 'setInterfaceId', 'iid', nil
end

CodeGenerator.generateChildListContainerClass do |modelDefinition|
    modelDefinition.className = 'LocomotiveList'
    modelDefinition.xmlClassName = 'lclist'
    modelDefinition.set_child_list 'mLocomotivesList', 'Locomotive', 'locomotivesList', 'setLocomotiveList', nil
end
