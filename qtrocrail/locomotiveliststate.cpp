/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotiveliststate.h"

#include "locomotive.h"
#include "serverstate.h"
#include "modelquery.h"
#include "remotequery.h"

#include <QStateMachine>
#include <QTimer>
#include <QDebug>

namespace QtRocrail
{

LocomotiveListState::LocomotiveListState(QSharedPointer<QtRocrail::ServerState> serverObject, QObject *parent)
    : QObject(parent), serverPointer(serverObject), knownLocomotives(), mIsValid(false), automaton(0)
{
    qDebug() << "LocomotiveListState::LocomotiveListState(" << serverPointer.data() << "," << parent << ")" << "==" << this;
    automaton = new QStateMachine(this);
    QState *notConnectedState = new QState(automaton);
    QState *connectedStateListEmpty = new QState(automaton);
    connect(connectedStateListEmpty, SIGNAL(entered()), this, SLOT(connectionReady()));
    QState *connectedAndListFull = new QState(automaton);

    notConnectedState->addTransition(serverPointer.data(), SIGNAL(connected()), connectedStateListEmpty);
    connectedStateListEmpty->addTransition(this, SIGNAL(listIsValid()), connectedAndListFull);
    connectedStateListEmpty->addTransition(serverPointer.data(), SIGNAL(disconnected()), notConnectedState);
    connectedAndListFull->addTransition(serverPointer.data(), SIGNAL(disconnected()), notConnectedState);

    connect(serverPointer.data(), SIGNAL(newLocomotiveMessage(QtRocrail::Locomotive)), this, SLOT(newLocomotiveMessage(QtRocrail::Locomotive)));
    connect(serverPointer.data(), SIGNAL(newLocomotiveListMessage(QtRocrail::LocomotiveList)), this, SLOT(newLocomotiveListMessage(QtRocrail::LocomotiveList)));
    connect(serverPointer.data(), SIGNAL(newModelQueryMessage(const QtRocrail::ModelQuery&)), this, SLOT(newModelQueryMessage(QtRocrail::ModelQuery)));
    connect(this, SIGNAL(newRequest(QtRocrail::RemoteQuery)), serverPointer.data(), SLOT(sendServerQuery(QtRocrail::RemoteQuery)));

    automaton->setInitialState(notConnectedState);
    automaton->start();
}

LocomotiveListState::~LocomotiveListState()
{
    qDebug() << "LocomotiveListState::~LocomotiveListState()";
}

void LocomotiveListState::newLocomotiveMessage(const Locomotive &locomotive)
{
    qDebug() << "LocomotiveListState::newLocomotiveMessage(const Locomotive &locomotive)";
    if (knownLocomotives.contains(locomotive.id())) {
        knownLocomotives[locomotive.id()].fuse(locomotive);
        emit locomotiveModified(knownLocomotives[locomotive.id()]);
    } else {
        if (!mIsValid && !knownLocomotives.contains(locomotive.id())) {
            knownLocomotives[locomotive.id()] = locomotive;
            emit locomotiveAdded(knownLocomotives[locomotive.id()]);
        }
    }
}

void LocomotiveListState::newModelQueryMessage(const QtRocrail::ModelQuery &modelQuery)
{
    qDebug() << "LocomotiveListState::newModelQueryMessage(const QtRocrail::ModelQuery &modelQuery)";
    if (modelQuery.command() == QtRocrail::ModelQuery::add) {
        if (modelQuery.locomotiveValid()) {
            if (!knownLocomotives.contains(modelQuery.locomotive().id())) {
                knownLocomotives[modelQuery.locomotive().id()] = modelQuery.locomotive();
                emit locomotiveAdded(knownLocomotives[modelQuery.locomotive().id()]);
            }
        }
    }
}

bool LocomotiveListState::isValid() const
{
    qDebug() << "LocomotiveListState::isValid() const";
    return mIsValid;
}

void LocomotiveListState::newLocomotiveListMessage(const LocomotiveList &locomotiveList)
{
    qDebug() << "LocomotiveListState::newLocomotiveListMessage(const LocomotiveList &locomotiveList)";
    for (QList<Locomotive>::const_iterator itLoco = locomotiveList.locomotivesList().begin(); itLoco != locomotiveList.locomotivesList().end(); ++itLoco) {
        newLocomotiveMessage(*itLoco);
    }
    emit listIsValid();
    for (QHash<QString, Locomotive>::iterator it = knownLocomotives.begin(); it != knownLocomotives.end(); ++it) {
        qDebug() << "existing locomotive" << it->id() << "at speed:" << it->speed();
    }
}

void LocomotiveListState::connectionReady()
{
    qDebug() << "LocomotiveListState::connectionReady()" << "ask locomotive list";
    emit newRequest(RemoteQuery::fromQuery(ModelQuery(ModelQuery::lclist, Locomotive())));
}

void LocomotiveListState::connectionLost()
{
    qDebug() << "LocomotiveListState::connectionLost()";
    mIsValid = false;
    emit validChanged();
}

const Locomotive& LocomotiveListState::locomotive(const QString &locomotiveId) const
{
    return knownLocomotives.find(locomotiveId).value();
}

bool LocomotiveListState::locomotiveExists(const QString &locomotiveId) const
{
    return knownLocomotives.contains(locomotiveId);
}

}
