/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined LOCOMOTIVELISTSTATE_H
#define LOCOMOTIVELISTSTATE_H

#include <QObject>
#include <QHash>
#include <QSharedPointer>

class QStateMachine;

#include "qtrocrail_export.h"

namespace QtRocrail
{

class RemoteQuery;

class LocomotiveList;

class Locomotive;

class ModelQuery;

class ServerState;

class QTROCRAIL_EXPORT LocomotiveListState : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(bool valid
               READ isValid
               NOTIFY validChanged
               )

    explicit LocomotiveListState(QSharedPointer<QtRocrail::ServerState> serverObject, QObject *parent = 0);

    ~LocomotiveListState();

    bool isValid() const;

    const Locomotive& locomotive(const QString&) const;

    bool locomotiveExists(const QString&) const;

protected slots:

    void newModelQueryMessage(const QtRocrail::ModelQuery &modelQuery);

    void newLocomotiveMessage(const QtRocrail::Locomotive &locomotive);

    void newLocomotiveListMessage(const QtRocrail::LocomotiveList &locomotiveList);

    void connectionReady();

    void connectionLost();

signals:

    void newRequest(const QtRocrail::RemoteQuery&);

    void validChanged();

    void locomotiveAdded(const QtRocrail::Locomotive&);

    void locomotiveModified(const QtRocrail::Locomotive&);

    void locomotiveRemoved(const QtRocrail::Locomotive&);

    void listIsValid();

private:

    QSharedPointer<QtRocrail::ServerState> serverPointer;

    QHash<QString, QtRocrail::Locomotive> knownLocomotives;

    bool mIsValid;

    QStateMachine *automaton;

};

}

#endif
