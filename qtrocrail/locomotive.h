/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined LOCOMOTIVE_H
#define LOCOMOTIVE_H

#include "qtrocrail_export.h"

#include <QtGlobal>
#include <QString>
#include <QMetaType>

class QDomDocument;
class QDomNode;

namespace QtRocrail
{


class QTROCRAIL_EXPORT Locomotive
{
public:
    explicit Locomotive(const QString&, const QString&, const QString&, int, bool, bool, int, int, int, int, int, int, bool, bool, int, const QString&, const QString&, int, const QString&, int, int, bool, const QString&, int, const QString&);

    explicit Locomotive(const QDomNode&);

    Locomotive();

    void toXML(QDomDocument&, QDomNode&) const;

    void fuse(const Locomotive&);

    const QString& id() const
    {
        return mId;
    }

    void setId(const QString &pmId)
    {
        mId = pmId;
        mIdValid = 1;
    }

    const QString& previousId() const
    {
        return mPreviousId;
    }

    void setPreviousId(const QString &pmPreviousId)
    {
        mPreviousId = pmPreviousId;
        mPreviousIdValid = 1;
    }

    const QString& shortId() const
    {
        return mShortId;
    }

    void setShortId(const QString &pmShortId)
    {
        mShortId = pmShortId;
        mShortIdValid = 1;
    }

    int speed() const
    {
        return mCurrentSpeed;
    }

    void setSpeed(int pmCurrentSpeed)
    {
        mCurrentSpeed = pmCurrentSpeed;
        mCurrentSpeedValid = 1;
    }

    bool lightOn() const
    {
        return mLightOn;
    }

    void setLightOn(bool pmLightOn)
    {
        mLightOn = pmLightOn;
        mLightOnValid = 1;
    }

    bool directionIsForward() const
    {
        return mDirectionIsForward;
    }

    void setDirectionIsForward(bool pmDirectionIsForward)
    {
        mDirectionIsForward = pmDirectionIsForward;
        mDirectionIsForwardValid = 1;
    }

    int minSpeed() const
    {
        return mMinSpeed;
    }

    void setMinSpeed(int pmMinSpeed)
    {
        mMinSpeed = pmMinSpeed;
        mMinSpeedValid = 1;
    }

    int mediumSpeed() const
    {
        return mMediumSpeed;
    }

    void setMediumSpeed(int pmMediumSpeed)
    {
        mMediumSpeed = pmMediumSpeed;
        mMediumSpeedValid = 1;
    }

    int maxSpeed() const
    {
        return mMaxSpeed;
    }

    void setMaxSpeed(int pmMaxSpeed)
    {
        mMaxSpeed = pmMaxSpeed;
        mMaxSpeedValid = 1;
    }

    int reverseMinSpeed() const
    {
        return mReverseMinSpeed;
    }

    void setReverseMinSpeed(int pmReverseMinSpeed)
    {
        mReverseMinSpeed = pmReverseMinSpeed;
        mReverseMinSpeedValid = 1;
    }

    int reverseMediumSpeed() const
    {
        return mReverseMediumSpeed;
    }

    void setReverseMediumSpeed(int pmReverseMediumSpeed)
    {
        mReverseMediumSpeed = pmReverseMediumSpeed;
        mReverseMediumSpeedValid = 1;
    }

    int reverseMaxSpeed() const
    {
        return mReverseMaxSpeed;
    }

    void setReverseMaxSpeed(int pmReverseMaxSpeed)
    {
        mReverseMaxSpeed = pmReverseMaxSpeed;
        mReverseMaxSpeedValid = 1;
    }

    bool useScheduleTime() const
    {
        return mUseScheduleTime;
    }

    void setUseScheduleTime(bool pmUseScheduleTime)
    {
        mUseScheduleTime = pmUseScheduleTime;
        mUseScheduleTimeValid = 1;
    }

    bool show() const
    {
        return mShow;
    }

    void setShow(bool pmShow)
    {
        mShow = pmShow;
        mShowValid = 1;
    }

    int bus() const
    {
        return mBus;
    }

    void setBus(int pmBus)
    {
        mBus = pmBus;
        mBusValid = 1;
    }

    const QString& description() const
    {
        return mDescription;
    }

    void setDescription(const QString &pmDescription)
    {
        mDescription = pmDescription;
        mDescriptionValid = 1;
    }

    const QString& documentation() const
    {
        return mDocumentation;
    }

    void setDocumentation(const QString &pmDocumentation)
    {
        mDocumentation = pmDocumentation;
        mDocumentationValid = 1;
    }

    int adress() const
    {
        return mAdress;
    }

    void setAdress(int pmAdress)
    {
        mAdress = pmAdress;
        mAdressValid = 1;
    }

    const QString& roadName() const
    {
        return mRoadName;
    }

    void setRoadName(const QString &pmRoadName)
    {
        mRoadName = pmRoadName;
        mRoadNameValid = 1;
    }

    int length() const
    {
        return mLength;
    }

    void setLength(int pmLength)
    {
        mLength = pmLength;
        mLengthValid = 1;
    }

    int identifier() const
    {
        return mIdentifier;
    }

    void setIdentifier(int pmIdentifier)
    {
        mIdentifier = pmIdentifier;
        mIdentifierValid = 1;
    }

    bool useShortId() const
    {
        return mUseShortId;
    }

    void setUseShortId(bool pmUseShortId)
    {
        mUseShortId = pmUseShortId;
        mUseShortIdValid = 1;
    }

    const QString& protocol() const
    {
        return mProtocol;
    }

    void setProtocol(const QString &pmProtocol)
    {
        mProtocol = pmProtocol;
        mProtocolValid = 1;
    }

    int protocolVersion() const
    {
        return mProtocolVersion;
    }

    void setProtocolVersion(int pmProtocolVersion)
    {
        mProtocolVersion = pmProtocolVersion;
        mProtocolVersionValid = 1;
    }

    const QString& interfaceId() const
    {
        return mInterfaceId;
    }

    void setInterfaceId(const QString &pmInterfaceId)
    {
        mInterfaceId = pmInterfaceId;
        mInterfaceIdValid = 1;
    }

private:

    QString mId;

    QString mPreviousId;

    QString mShortId;

    int mCurrentSpeed;

    bool mLightOn;

    bool mDirectionIsForward;

    int mMinSpeed;

    int mMediumSpeed;

    int mMaxSpeed;

    int mReverseMinSpeed;

    int mReverseMediumSpeed;

    int mReverseMaxSpeed;

    bool mUseScheduleTime;

    bool mShow;

    int mBus;

    QString mDescription;

    QString mDocumentation;

    int mAdress;

    QString mRoadName;

    int mLength;

    int mIdentifier;

    bool mUseShortId;

    QString mProtocol;

    int mProtocolVersion;

    QString mInterfaceId;

    /**
     * Is the field mId is valid
     */
    int mIdValid:1;

    /**
     * Is the field mPreviousId is valid
     */
    int mPreviousIdValid:1;

    /**
     * Is the field mShortId is valid
     */
    int mShortIdValid:1;

    /**
     * Is the field mCurrentSpeed is valid
     */
    int mCurrentSpeedValid:1;

    /**
     * Is the field mLightOn is valid
     */
    int mLightOnValid:1;

    /**
     * Is the field mDirectionIsForward is valid
     */
    int mDirectionIsForwardValid:1;

    /**
     * Is the field mMinSpeed is valid
     */
    int mMinSpeedValid:1;

    /**
     * Is the field mMediumSpeed is valid
     */
    int mMediumSpeedValid:1;

    /**
     * Is the field mMaxSpeed is valid
     */
    int mMaxSpeedValid:1;

    /**
     * Is the field mReverseMinSpeed is valid
     */
    int mReverseMinSpeedValid:1;

    /**
     * Is the field mReverseMediumSpeed is valid
     */
    int mReverseMediumSpeedValid:1;

    /**
     * Is the field mReverseMaxSpeed is valid
     */
    int mReverseMaxSpeedValid:1;

    /**
     * Is the field mUseScheduleTime is valid
     */
    int mUseScheduleTimeValid:1;

    /**
     * Is the field mShow is valid
     */
    int mShowValid:1;

    /**
     * Is the field mBus is valid
     */
    int mBusValid:1;

    /**
     * Is the field mDescription is valid
     */
    int mDescriptionValid:1;

    /**
     * Is the field mDocumentation is valid
     */
    int mDocumentationValid:1;

    /**
     * Is the field mAdress is valid
     */
    int mAdressValid:1;

    /**
     * Is the field mRoadName is valid
     */
    int mRoadNameValid:1;

    /**
     * Is the field mLength is valid
     */
    int mLengthValid:1;

    /**
     * Is the field mIdentifier is valid
     */
    int mIdentifierValid:1;

    /**
     * Is the field mUseShortId is valid
     */
    int mUseShortIdValid:1;

    /**
     * Is the field mProtocol is valid
     */
    int mProtocolValid:1;

    /**
     * Is the field mProtocolVersion is valid
     */
    int mProtocolVersionValid:1;

    /**
     * Is the field mInterfaceId is valid
     */
    int mInterfaceIdValid:1;

};

}

Q_DECLARE_METATYPE(QtRocrail::Locomotive)

#endif

