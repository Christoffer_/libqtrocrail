/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "networkprotocol.h"
#include "remotequery.h"

#include <QAbstractSocket>
#include <QXmlStreamWriter>
#include <QStringList>

namespace QtRocrail
{

static const int XML_HEADER_SIZE = 71;

NetworkTransport::NetworkTransport(QAbstractSocket *device, QObject *parent) :
    QObject(parent), mDevice(device), mIsConnected(false),
    mHeaderRegExp("<\\?xml version=\\\"1\\.0\\\" encoding=\\\"UTF\\-8\\\"\\?>\\n\\s*<xmlh>\\n\\s*<xml size=\\\"([0-9]+)\\\"/>\\n\\s*</xmlh>"),
    mBuffer(), mCurrentMessageSize(-1)
{
    connect(device, SIGNAL(readyRead()), this, SLOT(dataReady()));
    connect(device, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(serverSocketStateChanged(QAbstractSocket::SocketState)));
    connect(device, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(serverSocketStateInError(QAbstractSocket::SocketError)));
    connect(device, SIGNAL(connected()), this, SLOT(serverSocketConnected()));
    connect(device, SIGNAL(disconnected()), this, SLOT(serverSocketDisconnected()));
}

NetworkTransport::~NetworkTransport()
{
    disconnect(mDevice, SIGNAL(readyRead()), this, SLOT(dataReady()));
    disconnect(mDevice, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(serverSocketStateChanged(QAbstractSocket::SocketState)));
    disconnect(mDevice, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(serverSocketStateInError(QAbstractSocket::SocketError)));
    disconnect(mDevice, SIGNAL(connected()), this, SLOT(serverSocketConnected()));
    disconnect(mDevice, SIGNAL(disconnected()), this, SLOT(serverSocketDisconnected()));
}

void NetworkTransport::sendQuery(const RemoteQuery &theQuery) const
{
    const QString &documentContent(theQuery.toString(0));
    const QString &headerContent(buildHeader(documentContent.length()));
    qDebug() << headerContent.toUtf8();
    mDevice->write(headerContent.toUtf8());
    qDebug() << documentContent.toUtf8();
    mDevice->write(documentContent.toUtf8());
}

void NetworkTransport::dataReady()
{
    mBuffer += mDevice->readAll();
    if (mHeaderRegExp.indexIn(mBuffer) != -1) {
        const QStringList &matches(mHeaderRegExp.capturedTexts());
        const QString &stringMessageSize = matches[1];
        mCurrentMessageSize = stringMessageSize.toInt() - 1;

        if (mBuffer.length() >= mHeaderRegExp.matchedLength() + mCurrentMessageSize) {
            mBuffer.remove(0, mHeaderRegExp.matchedLength());
            const QStringRef &currentMessage(mBuffer.leftRef(mCurrentMessageSize));
            receivedQuerys.push_front(RemoteQuery::fromStringRef(currentMessage));
            mBuffer.remove(0, mCurrentMessageSize);
            emit receivedQuery();
        }
    }
}

QString NetworkTransport::buildHeader(unsigned int length) const
{
    QString header;
    QXmlStreamWriter theWriter(&header);

    theWriter.writeStartDocument();
    theWriter.writeStartElement(QString::fromLatin1("xmlh"));
    theWriter.writeStartElement(QString::fromLatin1("xml"));
    theWriter.writeAttribute(QString::fromLatin1("size"), QString::number(length));
    theWriter.writeEndElement();
    theWriter.writeEndElement();

    return header;
}

bool NetworkTransport::containsQuerys() const
{
    return !receivedQuerys.empty();
}

RemoteQuery NetworkTransport::readLastQuery() const
{
    return receivedQuerys.last();
}

void NetworkTransport::popLastQuery()
{
    return receivedQuerys.pop_back();
}

void NetworkTransport::serverSocketStateChanged(QAbstractSocket::SocketState newState)
{
    qDebug() << "NetworkTransport::serverSocketStateChanged(QAbstractSocket::SocketState newState)" << newState;
}

void NetworkTransport::serverSocketStateInError(QAbstractSocket::SocketError theError)
{
    qDebug() << "NetworkTransport::serverSocketStateInError(QAbstractSocket::SocketError theError)" << theError;
    if (theError == QAbstractSocket::ConnectionRefusedError) {
        emit connectionError();
    }
}

void NetworkTransport::serverSocketConnected()
{
    qDebug() << "NetworkTransport::serverSocketConnected()" << "connected";
    mIsConnected = true;
    emit connected();
    emit connectStateChanged();
}

void NetworkTransport::serverSocketDisconnected()
{
    qDebug() << "NetworkTransport::serverSocketDisconnected()" << "disconnected";
    mIsConnected = false;
    emit disconnected();
    emit connectStateChanged();
}

void NetworkTransport::connectToHost(const QString &address, quint16 port, QIODevice::OpenMode openMode)
{
    mDevice->connectToHost(address, port, openMode);
}

void NetworkTransport::connectToHost(const QHostAddress &address, quint16 port, QIODevice::OpenMode openMode)
{
    mDevice->connectToHost(address, port, openMode);
}

bool NetworkTransport::isConnected() const
{
    return mIsConnected;
}

}
