/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "remotequery.h"

#include <QDebug>

namespace QtRocrail
{

RemoteQuery::RemoteQuery(const QDomDocument &xmlContent) : query(xmlContent)
{
}

RemoteQuery::RemoteQuery(const QString &stringContent) : query()
{
    query.setContent(stringContent);
}

RemoteQuery::RemoteQuery(const QStringRef &stringContent) : query()
{
    query.setContent(stringContent.toString());
}

QString RemoteQuery::toString(int indentation) const
{
    return query.toString(indentation);
}

RemoteQuery RemoteQuery::fromString(const QString &data)
{
    return RemoteQuery(data);
}

RemoteQuery RemoteQuery::fromStringRef(const QStringRef &data)
{
    return RemoteQuery(data);
}

const QDomDocument& RemoteQuery::xml() const
{
    return query;
}

NetworkTransport::MessageType RemoteQuery::getHeader() const
{
    if (query.firstChild().nodeName() == QLatin1String("lclist")) {
        return NetworkTransport::lclist;
    }
    if (query.firstChild().nodeName() == QLatin1String("lc")) {
        return NetworkTransport::lc;
    }
    if (query.firstChild().nodeName() == QLatin1String("clock")) {
        return NetworkTransport::clocktick;
    }
    if (query.firstChild().nodeName() == QLatin1String("model")) {
        return NetworkTransport::model;
    }

    return NetworkTransport::invalid;
}

}
