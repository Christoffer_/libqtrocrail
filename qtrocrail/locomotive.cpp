/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotive.h"

#include <QDomDocument>
#include <QDomNode>
#include <QDebug>

namespace QtRocrail
{

Locomotive::Locomotive()
    : mId(), mPreviousId(), mShortId(), mCurrentSpeed(), mLightOn(), mDirectionIsForward(), mMinSpeed(), mMediumSpeed(), mMaxSpeed(), mReverseMinSpeed(), mReverseMediumSpeed(), mReverseMaxSpeed(), mUseScheduleTime(), mShow(), mBus(), mDescription(), mDocumentation(), mAdress(), mRoadName(), mLength(), mIdentifier(), mUseShortId(), mProtocol(), mProtocolVersion(), mInterfaceId(), mIdValid(0), mPreviousIdValid(0), mShortIdValid(0), mCurrentSpeedValid(0), mLightOnValid(0), mDirectionIsForwardValid(0), mMinSpeedValid(0), mMediumSpeedValid(0), mMaxSpeedValid(0), mReverseMinSpeedValid(0), mReverseMediumSpeedValid(0), mReverseMaxSpeedValid(0), mUseScheduleTimeValid(0), mShowValid(0), mBusValid(0), mDescriptionValid(0), mDocumentationValid(0), mAdressValid(0), mRoadNameValid(0), mLengthValid(0), mIdentifierValid(0), mUseShortIdValid(0), mProtocolValid(0), mProtocolVersionValid(0), mInterfaceIdValid(0)
{
}

Locomotive::Locomotive(const QString &pmId, const QString &pmPreviousId, const QString &pmShortId, int pmCurrentSpeed, bool pmLightOn, bool pmDirectionIsForward, int pmMinSpeed, int pmMediumSpeed, int pmMaxSpeed, int pmReverseMinSpeed, int pmReverseMediumSpeed, int pmReverseMaxSpeed, bool pmUseScheduleTime, bool pmShow, int pmBus, const QString &pmDescription, const QString &pmDocumentation, int pmAdress, const QString &pmRoadName, int pmLength, int pmIdentifier, bool pmUseShortId, const QString &pmProtocol, int pmProtocolVersion, const QString &pmInterfaceId)
    : mId(pmId), mPreviousId(pmPreviousId), mShortId(pmShortId), mCurrentSpeed(pmCurrentSpeed), mLightOn(pmLightOn), mDirectionIsForward(pmDirectionIsForward), mMinSpeed(pmMinSpeed), mMediumSpeed(pmMediumSpeed), mMaxSpeed(pmMaxSpeed), mReverseMinSpeed(pmReverseMinSpeed), mReverseMediumSpeed(pmReverseMediumSpeed), mReverseMaxSpeed(pmReverseMaxSpeed), mUseScheduleTime(pmUseScheduleTime), mShow(pmShow), mBus(pmBus), mDescription(pmDescription), mDocumentation(pmDocumentation), mAdress(pmAdress), mRoadName(pmRoadName), mLength(pmLength), mIdentifier(pmIdentifier), mUseShortId(pmUseShortId), mProtocol(pmProtocol), mProtocolVersion(pmProtocolVersion), mInterfaceId(pmInterfaceId), mIdValid(1), mPreviousIdValid(1), mShortIdValid(1), mCurrentSpeedValid(1), mLightOnValid(1), mDirectionIsForwardValid(1), mMinSpeedValid(1), mMediumSpeedValid(1), mMaxSpeedValid(1), mReverseMinSpeedValid(1), mReverseMediumSpeedValid(1), mReverseMaxSpeedValid(1), mUseScheduleTimeValid(1), mShowValid(1), mBusValid(1), mDescriptionValid(1), mDocumentationValid(1), mAdressValid(1), mRoadNameValid(1), mLengthValid(1), mIdentifierValid(1), mUseShortIdValid(1), mProtocolValid(1), mProtocolVersionValid(1), mInterfaceIdValid(1)
{
}

Locomotive::Locomotive(const QDomNode &source)
    : mId(), mPreviousId(), mShortId(), mCurrentSpeed(), mLightOn(), mDirectionIsForward(), mMinSpeed(), mMediumSpeed(), mMaxSpeed(), mReverseMinSpeed(), mReverseMediumSpeed(), mReverseMaxSpeed(), mUseScheduleTime(), mShow(), mBus(), mDescription(), mDocumentation(), mAdress(), mRoadName(), mLength(), mIdentifier(), mUseShortId(), mProtocol(), mProtocolVersion(), mInterfaceId()
{
    if (source.nodeName() == QString::fromLatin1("lc"))
    {
        {
            const QString &attributeName(QString::fromLatin1("id"));
            if (source.attributes().contains(attributeName))
            {
                mId = source.attributes().namedItem(attributeName).nodeValue();
                mIdValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("prev_id"));
            if (source.attributes().contains(attributeName))
            {
                mPreviousId = source.attributes().namedItem(attributeName).nodeValue();
                mPreviousIdValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("shortid"));
            if (source.attributes().contains(attributeName))
            {
                mShortId = source.attributes().namedItem(attributeName).nodeValue();
                mShortIdValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V"));
            if (source.attributes().contains(attributeName))
            {
                mCurrentSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mCurrentSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("fn"));
            if (source.attributes().contains(attributeName))
            {
                mLightOn = source.attributes().namedItem(attributeName).nodeValue() == "true" ? true : false;
                mLightOnValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("dir"));
            if (source.attributes().contains(attributeName))
            {
                mDirectionIsForward = source.attributes().namedItem(attributeName).nodeValue() == "true" ? true : false;
                mDirectionIsForwardValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_min"));
            if (source.attributes().contains(attributeName))
            {
                mMinSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mMinSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_mid"));
            if (source.attributes().contains(attributeName))
            {
                mMediumSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mMediumSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_max"));
            if (source.attributes().contains(attributeName))
            {
                mMaxSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mMaxSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_Rmin"));
            if (source.attributes().contains(attributeName))
            {
                mReverseMinSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mReverseMinSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_Rmid"));
            if (source.attributes().contains(attributeName))
            {
                mReverseMediumSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mReverseMediumSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("V_Rmax"));
            if (source.attributes().contains(attributeName))
            {
                mReverseMaxSpeed = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mReverseMaxSpeedValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("usescheduletime"));
            if (source.attributes().contains(attributeName))
            {
                mUseScheduleTime = source.attributes().namedItem(attributeName).nodeValue() == "true" ? true : false;
                mUseScheduleTimeValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("show"));
            if (source.attributes().contains(attributeName))
            {
                mShow = source.attributes().namedItem(attributeName).nodeValue() == "true" ? true : false;
                mShowValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("bus"));
            if (source.attributes().contains(attributeName))
            {
                mBus = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mBusValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("desc"));
            if (source.attributes().contains(attributeName))
            {
                mDescription = source.attributes().namedItem(attributeName).nodeValue();
                mDescriptionValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("docu"));
            if (source.attributes().contains(attributeName))
            {
                mDocumentation = source.attributes().namedItem(attributeName).nodeValue();
                mDocumentationValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("addr"));
            if (source.attributes().contains(attributeName))
            {
                mAdress = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mAdressValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("roadname"));
            if (source.attributes().contains(attributeName))
            {
                mRoadName = source.attributes().namedItem(attributeName).nodeValue();
                mRoadNameValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("len"));
            if (source.attributes().contains(attributeName))
            {
                mLength = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mLengthValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("identifier"));
            if (source.attributes().contains(attributeName))
            {
                mIdentifier = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mIdentifierValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("useshortid"));
            if (source.attributes().contains(attributeName))
            {
                mUseShortId = source.attributes().namedItem(attributeName).nodeValue() == "true" ? true : false;
                mUseShortIdValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("prot"));
            if (source.attributes().contains(attributeName))
            {
                mProtocol = source.attributes().namedItem(attributeName).nodeValue();
                mProtocolValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("protver"));
            if (source.attributes().contains(attributeName))
            {
                mProtocolVersion = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mProtocolVersionValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("iid"));
            if (source.attributes().contains(attributeName))
            {
                mInterfaceId = source.attributes().namedItem(attributeName).nodeValue();
                mInterfaceIdValid = 1;
            }
        }
    }
}

void Locomotive::toXML(QDomDocument &document, QDomNode &rootNode) const
{
    QDomElement modelXmlQuery(document.createElement(QString::fromLatin1("lc")));
    if (mIdValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("id"), id());
    }
    if (mPreviousIdValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("prev_id"), previousId());
    }
    if (mShortIdValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("shortid"), shortId());
    }
    if (mCurrentSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V"), speed());
    }
    if (mLightOnValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("fn"), lightOn() ? QLatin1String("true") : QLatin1String("false"));
    }
    if (mDirectionIsForwardValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("dir"), directionIsForward() ? QLatin1String("true") : QLatin1String("false"));
    }
    if (mMinSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_min"), minSpeed());
    }
    if (mMediumSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_mid"), mediumSpeed());
    }
    if (mMaxSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_max"), maxSpeed());
    }
    if (mReverseMinSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_Rmin"), reverseMinSpeed());
    }
    if (mReverseMediumSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_Rmid"), reverseMediumSpeed());
    }
    if (mReverseMaxSpeedValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("V_Rmax"), reverseMaxSpeed());
    }
    if (mUseScheduleTimeValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("usescheduletime"), useScheduleTime() ? QLatin1String("true") : QLatin1String("false"));
    }
    if (mShowValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("show"), show() ? QLatin1String("true") : QLatin1String("false"));
    }
    if (mBusValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("bus"), bus());
    }
    if (mDescriptionValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("desc"), description());
    }
    if (mDocumentationValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("docu"), documentation());
    }
    if (mAdressValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("addr"), adress());
    }
    if (mRoadNameValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("roadname"), roadName());
    }
    if (mLengthValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("len"), length());
    }
    if (mIdentifierValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("identifier"), identifier());
    }
    if (mUseShortIdValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("useshortid"), useShortId() ? QLatin1String("true") : QLatin1String("false"));
    }
    if (mProtocolValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("prot"), protocol());
    }
    if (mProtocolVersionValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("protver"), protocolVersion());
    }
    if (mInterfaceIdValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("iid"), interfaceId());
    }
    rootNode.appendChild(modelXmlQuery);
}

void Locomotive::fuse(const Locomotive &other)
{
    if (other.mIdValid) {
        setId(other.id());
    }
    if (other.mPreviousIdValid) {
        setPreviousId(other.previousId());
    }
    if (other.mShortIdValid) {
        setShortId(other.shortId());
    }
    if (other.mCurrentSpeedValid) {
        setSpeed(other.speed());
    }
    if (other.mLightOnValid) {
        setLightOn(other.lightOn());
    }
    if (other.mDirectionIsForwardValid) {
        setDirectionIsForward(other.directionIsForward());
    }
    if (other.mMinSpeedValid) {
        setMinSpeed(other.minSpeed());
    }
    if (other.mMediumSpeedValid) {
        setMediumSpeed(other.mediumSpeed());
    }
    if (other.mMaxSpeedValid) {
        setMaxSpeed(other.maxSpeed());
    }
    if (other.mReverseMinSpeedValid) {
        setReverseMinSpeed(other.reverseMinSpeed());
    }
    if (other.mReverseMediumSpeedValid) {
        setReverseMediumSpeed(other.reverseMediumSpeed());
    }
    if (other.mReverseMaxSpeedValid) {
        setReverseMaxSpeed(other.reverseMaxSpeed());
    }
    if (other.mUseScheduleTimeValid) {
        setUseScheduleTime(other.useScheduleTime());
    }
    if (other.mShowValid) {
        setShow(other.show());
    }
    if (other.mBusValid) {
        setBus(other.bus());
    }
    if (other.mDescriptionValid) {
        setDescription(other.description());
    }
    if (other.mDocumentationValid) {
        setDocumentation(other.documentation());
    }
    if (other.mAdressValid) {
        setAdress(other.adress());
    }
    if (other.mRoadNameValid) {
        setRoadName(other.roadName());
    }
    if (other.mLengthValid) {
        setLength(other.length());
    }
    if (other.mIdentifierValid) {
        setIdentifier(other.identifier());
    }
    if (other.mUseShortIdValid) {
        setUseShortId(other.useShortId());
    }
    if (other.mProtocolValid) {
        setProtocol(other.protocol());
    }
    if (other.mProtocolVersionValid) {
        setProtocolVersion(other.protocolVersion());
    }
    if (other.mInterfaceIdValid) {
        setInterfaceId(other.interfaceId());
    }
}

}

