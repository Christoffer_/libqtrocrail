/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "clocktick.h"

#include <QDomDocument>
#include <QDomNode>
#include <QDebug>

namespace QtRocrail
{

static QLatin1String toString(ClockTick::Command value)
{
    switch(value)
    {
    case ClockTick::sync:
        return QLatin1String("sync");
        break;
    case ClockTick::invalid:
        return QLatin1String("invalid");
        break;
    }

    return QLatin1String("");
}

static ClockTick::Command fromString(const QString &value)
{
    if (value == QLatin1String("sync")) {
        return ClockTick::sync;
    }
    return ClockTick::invalid;
}

ClockTick::ClockTick()
    : mDivider(), mClockTime(), mCommand(), mDividerValid(0), mClockTimeValid(0), mCommandValid(0)
{
}

ClockTick::ClockTick(int pmDivider, qlonglong pmClockTime, Command pmCommand)
    : mDivider(pmDivider), mClockTime(pmClockTime), mCommand(pmCommand), mDividerValid(1), mClockTimeValid(1), mCommandValid(1)
{
}

ClockTick::ClockTick(const QDomNode &source)
    : mDivider(), mClockTime(), mCommand()
{
    if (source.nodeName() == QString::fromLatin1("clock"))
    {
        {
            const QString &attributeName(QString::fromLatin1("divider"));
            if (source.attributes().contains(attributeName))
            {
                mDivider = source.attributes().namedItem(attributeName).nodeValue().toInt();
                mDividerValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("time"));
            if (source.attributes().contains(attributeName))
            {
                mClockTime = source.attributes().namedItem(attributeName).nodeValue().toLongLong();
                mClockTimeValid = 1;
            }
        }
        {
            const QString &attributeName(QString::fromLatin1("cmd"));
            if (source.attributes().contains(attributeName))
            {
                mCommand = fromString(source.attributes().namedItem(attributeName).nodeValue());
                mCommandValid = 1;
            }
        }
    }
}

void ClockTick::toXML(QDomDocument &document, QDomNode &rootNode) const
{
    QDomElement modelXmlQuery(document.createElement(QString::fromLatin1("clock")));
    if (mDividerValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("divider"), divider());
    }
    if (mClockTimeValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("time"), clockTime());
    }
    if (mCommandValid) {
        modelXmlQuery.setAttribute(QString::fromLatin1("cmd"), toString(command()));
    }
    rootNode.appendChild(modelXmlQuery);
}

void ClockTick::fuse(const ClockTick &other)
{
    if (other.mDividerValid) {
        setDivider(other.divider());
    }
    if (other.mClockTimeValid) {
        setClockTime(other.clockTime());
    }
    if (other.mCommandValid) {
        setCommand(other.command());
    }
}

}

