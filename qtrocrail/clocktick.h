/**
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined CLOCKTICK_H
#define CLOCKTICK_H

#include "qtrocrail_export.h"

#include <QtGlobal>
#include <QString>
#include <QMetaType>

class QDomDocument;
class QDomNode;

namespace QtRocrail
{


class QTROCRAIL_EXPORT ClockTick
{
public:
    enum Command
    {
        sync, invalid
    };

    explicit ClockTick(int, qlonglong, Command);

    explicit ClockTick(const QDomNode&);

    ClockTick();

    void toXML(QDomDocument&, QDomNode&) const;

    void fuse(const ClockTick&);

    int divider() const
    {
        return mDivider;
    }

    void setDivider(int pmDivider)
    {
        mDivider = pmDivider;
        mDividerValid = 1;
    }

    qlonglong clockTime() const
    {
        return mClockTime;
    }

    void setClockTime(qlonglong pmClockTime)
    {
        mClockTime = pmClockTime;
        mClockTimeValid = 1;
    }

    Command command() const
    {
        return mCommand;
    }

    void setCommand(Command pmCommand)
    {
        mCommand = pmCommand;
        mCommandValid = 1;
    }

private:

    int mDivider;

    qlonglong mClockTime;

    Command mCommand;

    /**
     * Is the field mDivider is valid
     */
    int mDividerValid:1;

    /**
     * Is the field mClockTime is valid
     */
    int mClockTimeValid:1;

    /**
     * Is the field mCommand is valid
     */
    int mCommandValid:1;

};

}

Q_DECLARE_METATYPE(QtRocrail::ClockTick)

#endif

