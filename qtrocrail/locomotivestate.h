/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#if !defined LOCOMOTIVESTATE_H
#define LOCOMOTIVESTATE_H

#include "locomotive.h"

#include <QObject>
#include <QHash>
#include <QSharedPointer>
#include <QWeakPointer>

class QStateMachine;

#include "qtrocrail_export.h"

namespace QtRocrail
{

class RemoteQuery;

class ModelQuery;

class ServerState;

class LocomotiveListState;

class QTROCRAIL_EXPORT LocomotiveState : public QObject
{
    Q_OBJECT
public:

    Q_PROPERTY(bool valid
               READ isValid
               NOTIFY validChanged
               )

    explicit LocomotiveState(QSharedPointer<QtRocrail::ServerState> serverObject,
                             QWeakPointer<QtRocrail::LocomotiveListState> listPointer,
                             const QString &locomotiveId,
                             QObject *parent = 0);

    ~LocomotiveState();

    bool isValid() const;

    const Locomotive& locomotive() const;

    QWeakPointer<QtRocrail::ServerState> serverConnection() const;

protected slots:

    void newLocomotiveMessage(const QtRocrail::Locomotive &locomotive);

    void connectionReady();

    void connectionLost();

    void stateChangeLocomotiveListInvalidLocomotiveInvalid();

    void stateChangeLocomotiveListInvalidLocomotiveValid();

signals:

    void newRequest(const QtRocrail::RemoteQuery&);

    void validChanged();

    void locomotiveModified(const QtRocrail::Locomotive&);

    void listIsValid();

    void listIsInvalid();

private:
    void buildAutomaton();

    QSharedPointer<QtRocrail::ServerState> serverPointer;

    QWeakPointer<QtRocrail::LocomotiveListState> locomotiveListPointer;

    const QString &id;

    QtRocrail::Locomotive mLocomotive;

    bool mIsValid;

    QStateMachine *automaton;

};

}

#endif
