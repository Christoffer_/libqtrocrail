/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivejob.h"

#include <QLatin1String>

#include "locomotivestate.h"
#include "serverstate.h"
#include "remotequery.h"
#include "locomotive.h"

#include <KDebug>

LocomotiveJob::LocomotiveJob(QSharedPointer<QtRocrail::LocomotiveState> locomotiveState, const QString &destination,
                             const QString &operation, const QMap<QString, QVariant> &parameters, QObject *parent) :
    Plasma::ServiceJob(destination, operation, parameters, parent),
    mLocomotiveState(locomotiveState), mServerConnection(locomotiveState->serverConnection())
{
    kDebug();
}

void LocomotiveJob::start()
{
    if (operationName() == QLatin1String("SetSpeed")) {
        QtRocrail::Locomotive myLocomotiveMessage;
        myLocomotiveMessage.setId(destination());
        myLocomotiveMessage.setSpeed(parameters()["speed"].toInt());
        QDomDocument theXmlDoc;
        myLocomotiveMessage.toXML(theXmlDoc, theXmlDoc);
        QtRocrail::RemoteQuery myQuery(theXmlDoc);
        mServerConnection->sendServerQuery(myQuery);
        setResult(true);
        emitResult();
    }
    if (operationName() == QLatin1String("SwitchDirection")) {
        QtRocrail::Locomotive myLocomotiveMessage;
        myLocomotiveMessage.setId(destination());
        myLocomotiveMessage.setDirectionIsForward(!mLocomotiveState->locomotive().directionIsForward());
        QDomDocument theXmlDoc;
        myLocomotiveMessage.toXML(theXmlDoc, theXmlDoc);
        QtRocrail::RemoteQuery myQuery(theXmlDoc);
        mServerConnection->sendServerQuery(myQuery);
        setResult(true);
        emitResult();
    }
    if (operationName() == QLatin1String("SwithLight")) {
        QtRocrail::Locomotive myLocomotiveMessage;
        myLocomotiveMessage.setId(destination());
        myLocomotiveMessage.setLightOn(!mLocomotiveState->locomotive().lightOn());
        QDomDocument theXmlDoc;
        myLocomotiveMessage.toXML(theXmlDoc, theXmlDoc);
        QtRocrail::RemoteQuery myQuery(theXmlDoc);
        mServerConnection->sendServerQuery(myQuery);
        setResult(true);
        emitResult();
    }
}
