/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivelistsource.h"

#include "locomotive.h"
#include "locomotiveliststate.h"

#include <KDebug>

LocomotiveListSource::LocomotiveListSource(const QString &myUniqueName, QSharedPointer<QtRocrail::LocomotiveListState> dataSource, QObject *parent) :
    Plasma::DataContainer(parent), listSource(dataSource), uniqueName(myUniqueName)
{
    setObjectName(QLatin1String("locomotiveList:address:") + myUniqueName);
    connect(listSource.data(), SIGNAL(locomotiveAdded(QtRocrail::Locomotive)), this, SLOT(locomotiveIsAdded(QtRocrail::Locomotive)));
    connect(listSource.data(), SIGNAL(locomotiveRemoved(QtRocrail::Locomotive)), this, SLOT(locomotiveIsRemoved(QtRocrail::Locomotive)));
    kDebug() << "create source";
}

LocomotiveListSource::~LocomotiveListSource()
{
    kDebug() << "";
}

void LocomotiveListSource::locomotiveIsAdded(const QtRocrail::Locomotive &newLocomotive)
{
    kDebug() << newLocomotive.id();
    setData(newLocomotive.id(), QLatin1String("locomotive:") + newLocomotive.id() + QLatin1String(":") + uniqueName);
    checkForUpdate();
}

void LocomotiveListSource::locomotiveIsRemoved(const QtRocrail::Locomotive &newLocomotive)
{
    kDebug() << newLocomotive.id();
    setData(newLocomotive.id(), QVariant());
    checkForUpdate();
}
