/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "locomotivesource.h"

#include <remotequery.h>
#include <modelquery.h>
#include <serverstate.h>

#include <KDebug>
#include <QStateMachine>
#include <QDomDocument>

LocomotiveSource::LocomotiveSource(const QString &myUniqueName, const QString &locomotiveId, QtRocrail::LocomotiveState *dataSource, QObject *parent) :
    Plasma::DataContainer(parent), serverConnection(dataSource->serverConnection()), currentLocomotiveState(dataSource), uniqueName(myUniqueName)
{
    setObjectName(QLatin1String("locomotive:") + locomotiveId + QLatin1String(":") + myUniqueName);
    currentLocomotive.setId(locomotiveId);
    kDebug() << "create source for locomotive:" << locomotiveId;
    if (currentLocomotiveState->isValid()) {
        locomotiveIsModified(currentLocomotiveState->locomotive());
    } else {
        kDebug() << "locomotiveState is not valid";
    }
    kDebug() << "create source";
    connect(currentLocomotiveState.data(), SIGNAL(locomotiveModified(const QtRocrail::Locomotive&)), this, SLOT(locomotiveIsModified(QtRocrail::Locomotive)));
}

LocomotiveSource::~LocomotiveSource()
{
    kDebug() << "";
}

void LocomotiveSource::locomotiveIsModified(const QtRocrail::Locomotive &newLocomotive)
{
    currentLocomotive = newLocomotive;
    kDebug() << currentLocomotive.id();
    setData(QLatin1String("id"), currentLocomotive.id());
    setData(QLatin1String("speed"), currentLocomotive.speed());
    setData(QLatin1String("isForward"), currentLocomotive.directionIsForward());
    setData(QLatin1String("lightOn"), currentLocomotive.lightOn());
    checkForUpdate();
}

void LocomotiveSource::askLocomotiveInformation()
{
    QtRocrail::ModelQuery queryLocomotive;
    QDomDocument theDocument;
    queryLocomotive.setCommand(QtRocrail::ModelQuery::lcprops);
    queryLocomotive.toXML(theDocument, theDocument);
    QtRocrail::RemoteQuery myQuery(theDocument);
    serverConnection->sendServerQuery(myQuery);
}
