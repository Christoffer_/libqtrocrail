/*
 *   Copyright 2011 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LOCOMOTIVELISTSOURCE_H
#define LOCOMOTIVELISTSOURCE_H

#include <Plasma/DataContainer>
#include <QSharedPointer>

namespace QtRocrail
{
class LocomotiveListState;

class Locomotive;
}

class LocomotiveListSource : public Plasma::DataContainer
{
    Q_OBJECT

public:
    LocomotiveListSource(const QString&, QSharedPointer<QtRocrail::LocomotiveListState> dataSource, QObject *parent = 0);

    virtual ~LocomotiveListSource();

private slots:

    void locomotiveIsAdded(const QtRocrail::Locomotive&);

    void locomotiveIsRemoved(const QtRocrail::Locomotive&);

private:

    QSharedPointer<QtRocrail::LocomotiveListState> listSource;

    QString uniqueName;
};

#include <locomotiveliststate.h>

#endif // LOCOMOTIVELISTSOURCE_H
