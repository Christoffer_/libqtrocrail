/*
 *   Copyright 2010 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 3, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <modelquery.h>
#include <locomotive.h>
#include <networkprotocol.h>
#include <remotequery.h>

#include <QDate>
#include <QTcpSocket>
#include <QCoreApplication>
#include <KDebug>

using namespace QtRocrail;

int main(int argc, char *argv[])
{
    QCoreApplication myApp(argc, argv);

    kDebug() << "starting";

    QTcpSocket mySocket;
    mySocket.connectToHost("127.0.0.1", 62842, QIODevice::ReadWrite);
    kDebug() << "trying to connect";
    mySocket.waitForConnected();
    kDebug() << "connected";

    NetworkTransport *theProtocol = new NetworkTransport(&mySocket, &myApp);

    {
        ModelQuery myQuery(ModelQuery::lclist, Locomotive());
        
        QDomDocument theDocument;
        myQuery.toXML(theDocument, theDocument);

        RemoteQuery theQuery(theDocument);
        ModelQuery decodedQuery(theQuery.xml());

        theProtocol->sendQuery(theQuery);
    }

    {
        Locomotive theLocomotive;
        theLocomotive.setAdress(2);
        theLocomotive.setBus(0);
        theLocomotive.setDescription("test loco");
        theLocomotive.setDirectionIsForward(true);
        theLocomotive.setId(QLatin1String("NEW") + QTime::currentTime().toString());
        theLocomotive.setIdentifier(3);
        theLocomotive.setInterfaceId("test");
        theLocomotive.setLength(0);
        theLocomotive.setLightOn(false);
        theLocomotive.setMaxSpeed(100);
        theLocomotive.setMediumSpeed(40);
        theLocomotive.setMinSpeed(10);
        theLocomotive.setPreviousId("NEW2");
        theLocomotive.setReverseMaxSpeed(35);
        theLocomotive.setReverseMediumSpeed(15);
        theLocomotive.setReverseMinSpeed(3);
        theLocomotive.setShow(true);
        theLocomotive.setSpeed(0);
        theLocomotive.setUseScheduleTime(false);
        theLocomotive.setRoadName("test road");
        theLocomotive.setProtocol("P");
        theLocomotive.setProtocolVersion(1);
        theLocomotive.setUseShortId(false);

        ModelQuery addLocoQuery(ModelQuery::add, theLocomotive);

        QDomDocument theDocument;
        addLocoQuery.toXML(theDocument, theDocument);
        qDebug() << theDocument.toString();
        
        RemoteQuery theQuery(theDocument);
        ModelQuery decodedQuery(theQuery.xml());
        
        theProtocol->sendQuery(theQuery);
    }
    
    return myApp.exec();
}
